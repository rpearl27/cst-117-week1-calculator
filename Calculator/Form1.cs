﻿/**
 * Calculator Form
 * By:      Ron Pearl
 * Date:    6/6/17
 * 
 * Description: Form class that manages the calculator form. It holds the 
 * functionality of the button clicks and performs necessary calculations.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class canvas : Form
    {
        private double currentVal = 0;
        private String modifier = "";
        private bool modifierSelected = false;

        public canvas()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            currentVal = Double.Parse(this.textBoxTotal.Text);
            modifier = "+";
            modifierSelected = true;
        }

        private void buttonSubtract_Click(object sender, EventArgs e)
        {
            currentVal = Double.Parse(this.textBoxTotal.Text);
            modifier = "-";
            modifierSelected = true;
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            currentVal = Double.Parse(this.textBoxTotal.Text);
            modifier = "*";
            modifierSelected = true;
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            currentVal = Double.Parse(this.textBoxTotal.Text);
            modifier = "/";
            modifierSelected = true;
        }

        private void button0_Click(object sender, EventArgs e)
        {
            if (this.textBoxTotal.Text != "0")
                editTextBoxTotal("0");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            editTextBoxTotal("9");
        }

        private void buttonPeriod_Click(object sender, EventArgs e)
        {
            // Make sure there is not a period already, otherwise add it
            if ( ! this.textBoxTotal.Text.Contains('.'))
                editTextBoxTotal(".");
        }

        /**
         * Clear textBoxTotal and currentVal. Resets calculator.
         */
        private void buttonClear_Click(object sender, EventArgs e)
        {
            this.textBoxTotal.Text = "0";
            currentVal = 0;
        }

        private void buttonEquals_Click(object sender, EventArgs e)
        {
            double newVal;

            switch (modifier)
            {
                case "+":
                    newVal = currentVal + Double.Parse(this.textBoxTotal.Text);
                    this.textBoxTotal.Text = newVal.ToString("#.#########");
                    break;
                case "-":
                    newVal = currentVal - Double.Parse(this.textBoxTotal.Text);
                    this.textBoxTotal.Text = newVal.ToString("#.#########");
                    break;
                case "*":
                    newVal = currentVal * Double.Parse(this.textBoxTotal.Text);
                    this.textBoxTotal.Text = newVal.ToString("#.#########");
                    break;
                case "/":
                    newVal = currentVal / Double.Parse(this.textBoxTotal.Text);
                    this.textBoxTotal.Text = newVal.ToString("#.#########");
                    break;
                default:
                    break;
            }

            modifier = "";
        }

        /**
         * Takes a string value, checks if textBoxTotal currently a "0"
         * and updates the data respectively.
         */
        private void editTextBoxTotal(String val)
        {
            StringBuilder totalString = new StringBuilder();
            totalString.Append(this.textBoxTotal.Text);

            // First check if modifier selected to see if we need to clear textBoxTotal First
            if (modifierSelected)
            {
                this.textBoxTotal.Text = "0";
                modifierSelected = false;       // Reset bool so numbers stay showing
            }
            
            if (this.textBoxTotal.Text == "0")
            {
                this.textBoxTotal.Text = val;
            }
            else
            {
                totalString.Append(val);
                this.textBoxTotal.Text = totalString.ToString();
            }
        }
    }
}
